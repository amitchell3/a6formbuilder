import { Component, Input } from '@angular/core';
import { FormField, FieldChoice } from './app.component';

@Component({
    selector: 'app-single-select-editor',
    template: `<section provide-parent-form>
    <app-common-fields [field]="field"></app-common-fields>

    <div *ngIf="field.FieldType == 'DropDownList'">
        <label>
            <input type="checkbox" name="fieldRequired" [(ngModel)]="field.Required" /> Required Field
        </label>
    </div>

    <table>
        <thead>
            <tr>
                <th style="text-align: center;">Checked</th>
                <th>Label</th>
                <th>Value</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        <tr *ngFor="let choice of field.Choices; let i = index">
          <td>
            <input type="radio" name="{{'selected' + i}}" [checked]="choice.Selected" (click)="handleChoiceSelection(choice)"/>
          </td>
          <td>
            <input
                type="text"
                required
                name="{{'labelName' + i}}"
                ng-init="labelName = 'choice.Label' + $index"
                [(ngModel)]="choice.Label"
            />
          </td>
          <td>
            <input
                type="text"
                required
                name="{{'valueName' + i}}"
                ng-init="valueName = 'choice.Value' + $index"
                [(ngModel)]="choice.Value"
            />
          </td>
          <td>
            <button (click)="addChoice(i + 1)"><i>+</i></button>
            <button (click)="removeChoice(choice)" ng-hide="$first && $last"><i>-</i></button>
          </td>
        </tr>
        </tbody>
    </table>
</section>`
})

export class SingleSelectEditorComponent {
    @Input() field: FormField;

    handleChoiceSelection(choice) {
        if (choice.Selected === true) {
            choice.Selected = false;
        } else {
            this.field.Choices.forEach(c => {
                c.Selected = false;
            });
            choice.Selected = true;
        }
    }

    addChoice(index) {
        if (typeof index === 'undefined') {
            index = this.field.Choices.length + 1;
        }

        this.field.Choices.splice(index, 0, new FieldChoice('', '', false));
    }

    removeChoice(choice) {
        const index = this.field.Choices.indexOf(choice);
        this.field.Choices.splice(index, 1);
    }
}
