import { Component, Input } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-standard-field-editor',
    template: `<section>

    <div>
        <label>Field Label</label>
        <input type="text" [(ngModel)]="field.Label" disabled />
    </div>

    <div>
        <label>Field Name</label>
        <input type="text" [(ngModel)]="field.Name" disabled />
    </div>

    <div>
        <label><input type="checkbox" [(ngModel)]="field.Required" /> Required Field</label>
    </div>
</section>`
})

export class StandardFieldEditorComponent {
    @Input() field: FormField;
}
