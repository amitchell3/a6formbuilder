import { Component, Input, OnInit } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-textarea-viewer',
    template: `<div>
    <label>
        <span *ngIf="!field.Label">[Enter Label]</span>
        <span *ngIf="field.Label">{{ field.Label }}</span>
    </label>
    <textarea name="{{ field.Name }}" rows="{{ field.Rows }}" disabled></textarea>
</div>`
})

export class TextAreaViewerComponent {
    @Input() field: FormField;
}
