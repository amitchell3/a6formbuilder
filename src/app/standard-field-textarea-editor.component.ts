import { Component, Input } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-standard-field-textarea-editor',
    template: `<section>
    <div>
        <label>Field Label</label>
        <input type="text" [(ngModel)]="field.Label" disabled />
    </div>

    <div>
        <label>Field Name</label>
        <input type="text" [(ngModel)]="field.Name" disabled />
    </div>

    <div>
        <label>Number of rows</label>
        <input type="number" [(ngModel)]="field.Rows" min="2" />
    </div>

    <div>
        <label><input type="checkbox" [(ngModel)]="field.Required" /> Required Field</label>
    </div>
</section>`
})

export class StandardFieldTextAreaEditorComponent {
    @Input() field: FormField;
}
