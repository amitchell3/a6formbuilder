import { Component, Input, OnInit } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-single-check-box-viewer',
    template: `<div>
    <label>
        <input type="checkbox" name="{{ field.Name }}" ng-checked="field.Selected" style="margin-right: 8px;" disabled/>
        <span *ngIf="!field.Label">[Enter Label]</span>
        <span *ngIf="field.Label">{{ field.Label }}</span>
    </label>
</div>`
})

export class SingleCheckBoxViewerComponent {
    @Input() field: FormField;
}
