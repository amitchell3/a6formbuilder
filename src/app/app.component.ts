import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { HtmlService } from './html.service'

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent {
    title = 'formbuilder';

    public standardFieldTypes: FieldType[];
    public customFieldTypes: FieldType[];

    public form: FormBuilderForm;
    public html: string;

    public mode: string;
    public editingFormSettings: boolean;
    public currentField: FormField;
    public showFormHtml: boolean;

    constructor(private http: HttpClient, private htmlService: HtmlService) {
        this.showFormHtml = false;
        this.editingFormSettings = true;

        this.standardFieldTypes = [
            {
                Name: 'EmailAddress',
                DisplayName: 'Email Address',
                Icon: 'fa-at'
            },
            {
                Name: 'FirstName',
                DisplayName: 'First name',
                Icon: 'fa-user'
            },
            {
                Name: 'LastName',
                DisplayName: 'Last name',
                Icon: 'fa-user'
            },
            {
                Name: 'StreetAddress',
                DisplayName: 'Street Address',
                Icon: 'fa-home'
            },
            {
                Name: 'City',
                DisplayName: 'City',
                Icon: 'fa-home'
            },
            {
                Name: 'State',
                DisplayName: 'State',
                Icon: 'fa-home'
            },
            {
                Name: 'PostalCode',
                DisplayName: 'ZIP Code',
                Icon: 'fa-home'
            },
            {
                Name: 'Phone',
                DisplayName: 'Phone',
                Icon: 'fa-phone'
            },
            {
                Name: 'EmailOptIn',
                DisplayName: 'Email Opt In',
                Icon: 'fa-inbox'
            },
            {
                Name: 'Comments',
                DisplayName: 'Comments',
                Icon: 'fa-comment-o'
            }
        ];

        this.customFieldTypes = [
            {
                Name: 'TextBox',
                DisplayName: 'Text Box',
                Icon: 'fa-font'
            },
            {
                Name: 'TextArea',
                DisplayName: 'Paragraph Box',
                Icon: 'fa-paragraph'
            },
            {
                Name: 'DropDownList',
                DisplayName: 'Dropdown',
                Icon: 'fa-caret-square-o-down'
            },
            {
                Name: 'RadioButtonList',
                DisplayName: 'Radio Button List',
                Icon: 'fa-dot-circle-o'
            },
            {
                Name: 'SingleCheckBox',
                DisplayName: 'Checkbox',
                Icon: 'fa-check-square-o'
            },
            {
                Name: 'CheckBoxList',
                DisplayName: 'Checkbox List',
                Icon: 'fa-check-square-o'
            },
            {
                Name: 'FileUpload',
                DisplayName: 'File Upload',
                Icon: 'fa-file-o'
            }
        ];

        this.form = new FormBuilderForm(
            this.generateGuid(),
            'redirect', // successAction
            'text', // successContentFormat
            'Submit', // submitButtonText
            'trilogy-form-custom', // cssClass
            []
        );

        let editingFormId = new URLSearchParams(window.location.search).get('formId');
        this.mode = editingFormId ? "edit" : "add";

        if (this.mode == "edit") {
            this.http
                .get(`assets/json/form.json?id=${editingFormId}`)
                .subscribe((response: any) => {
                    if (response.result == null) {
                        this.mode = "invalid";
                    }
                    else {
                        this.form = response.result;
                    }
                });
        }

        // Get tenant hosts and name
        this.http
            .get("assets/json/tenant.json")
            .subscribe((response: any) => {
                this.form.Host = response.result.Hosts[0];
                this.form.Tenant = response.result.Tenant;
            });
    }

    addField(fieldType: string) {
        this.editingFormSettings = false;

        const field: FormField = {
            FieldType: fieldType,
            Required: false,
            Name: '',
            Label: '',
            Selected: false,
            Choices: [],
            Rows: 0
        };

        switch (fieldType) {
            case 'EmailAddress':
                field.Name = 'EmailAddress';
                field.Label = 'Email Address';
                break;
            case 'FirstName':
                field.Name = 'FirstName';
                field.Label = 'First Name';
                break;
            case 'LastName':
                field.Name = 'LastName';
                field.Label = 'Last Name';
                break;
            case 'StreetAddress':
                field.Name = 'StreetAddress';
                field.Label = 'Street Address';
                break;
            case 'State':
                field.Label = 'State';
                field.Name = 'StateProvince';
                break;
            case 'City':
                field.Label = 'City';
                field.Name = 'City';
                break;
            case 'PostalCode':
                field.Name = 'PostalCode';
                field.Label = 'ZIP Code';
                break;
            case 'Phone':
                field.Name = 'PhoneNumber';
                field.Label = 'Phone';
                break;
            case 'EmailOptIn':
                field.Name = 'EmailOptIn';
                field.Label = 'Email Opt In';
                field.Selected = false;
                break;
            case 'Comments':
                field.Name = 'Comments';
                field.Label = 'Comments';
                field.Rows = 4;
                break;
            case 'TextBox':
                field.Label = '';
                field.Name = '';
                break;
            case 'TextArea':
                field.Label = '';
                field.Name = '';
                field.Rows = 2;
                break;
            case 'DropDownList':
                field.Label = '';
                field.Name = '';
                field.Choices = [
                    {
                        Label: 'Choice #1',
                        Value: '',
                        Selected: false
                    },
                    {
                        Label: 'Choice #2',
                        Value: '',
                        Selected: false
                    }
                ];
                break;
            case 'RadioButtonList':
                field.Label = '';
                field.Name = '';
                field.Choices = [
                    {
                        Label: 'Choice #1',
                        Value: '',
                        Selected: false
                    },
                    {
                        Label: 'Choice #2',
                        Value: '',
                        Selected: false
                    }
                ];
                break;
            case 'SingleCheckBox':
                field.Label = '',
                    field.Name = '';
                field.Selected = false;
                break;
            case 'CheckBoxList':
                field.Label = '';
                field.Name = '';
                field.Choices = [
                    {
                        Label: 'Choice #1',
                        Value: '',
                        Selected: false
                    },
                    {
                        Label: 'Choice #2',
                        Value: '',
                        Selected: false
                    }
                ];
                break;
            case 'FileUpload':
                field.Label = '';
                field.Name = '';
                break;
        }

        let currentFieldIndex = 0;
        if (this.currentField) {
            currentFieldIndex = this.form.Fields.indexOf(this.currentField);
        }

        this.form.Fields.splice(currentFieldIndex + 1, 0, field);
        this.currentField = field;
    }

    removeField() {
        const index = this.form.Fields.indexOf(this.currentField);
        if (index >= 0) {
            this.form.Fields.splice(index, 1);
            if (this.form.Fields) {
                const newIndex = Math.max(0, index - 1);
                this.currentField = this.form.Fields[newIndex];
            } else {
                this.currentField = null;
            }
        }
    }

    setCurrentField(field: FormField, formIsValid: boolean) {
        if (formIsValid) {
            this.currentField = field;
            this.editingFormSettings = false;
        }
    }

    isFormSettingsEdittingActive() {
        if (this.editingFormSettings || this.form.Fields.length === 0) {
            return true;
        }

        if (!this.currentField) {
            return true;
        }

        return false;
    }

    isFieldUsed(fieldType: FieldType) {
        let isDisabled = null;
        this.form.Fields.forEach(f => {
            if (f.FieldType == fieldType.Name) {
                isDisabled = 'disabled';
            }
        });
        return isDisabled;
    }

    showCodeDialog(formIsValid: boolean) {
        if (!formIsValid) {
            return;
        }

        //ToDo Check if duplicates
        if (!this.checkForDuplicateFieldNames()) {
            this.html = this.htmlService.generateHtml(this.form);
            this.showFormHtml = true;
        }
    }

    checkForDuplicateFieldNames(): boolean {
        let duplicatesExits = false;
        // let invalidFields: FormField[] = [];

        this.form.Fields.forEach((field, index) => {
            let fieldName = field.Name;

            if (fieldName && !field.Duplicate) {
                this.form.Fields.forEach((matchField, matchIndex) => {
                    if (matchIndex != index && matchField.Name == fieldName) {
                        field.Duplicate = true;
                        matchField.Duplicate = true;
                        duplicatesExits = true;
                    }
                });

            }
        });

        return duplicatesExits;
    }

    private generateGuid() {
        return (
            this.S4() + this.S4() + '-'
            + this.S4() + '-4'
            + this.S4().substr(0, 3)
            + '-' + this.S4() + '-'
            + this.S4() + this.S4() + this.S4()
        ).toLowerCase();
    }

    private S4() {
        // tslint:disable-next-line:no-bitwise
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
}

export class FormBuilderForm {
    constructor(
        public id: string,
        public SuccessAction: string,
        public SuccessContentFormat: string,
        public SubmitButtonText: string,
        public CssClass: string,
        public Fields: FormField[],
        public Name?: string,
        public Tenant?: string,
        public Host?: string,
        public ActionCode?: string,
        public FormEntryGoal?: number,
        public FormEntryCountOffset?: number,
        public PublishFormEntries?: boolean,
        public SuccessContent?: string,
        public SuccessUrl?: string
    ) { }
}

export class FieldType {
    constructor(
        public Name: string,
        public DisplayName: string,
        public Icon: string
    ) { }
}

export class FormField {
    constructor(
        public FieldType: string,
        public Name: string,
        public Label: string,
        public Required: boolean,
        public Selected: boolean,
        public Choices: FieldChoice[],
        public Rows: number,
        public Duplicate?: boolean
    ) { }
}

export class FieldChoice {
    constructor(
        public Label: string,
        public Value: string,
        public Selected: boolean
    ) { }
}