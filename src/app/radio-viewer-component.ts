import { Component, Input, OnInit } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-radio-viewer',
    template: `<div>
    <label>
        <span *ngIf="!field.Label">[Enter Label]</span>
        <span *ngIf="field.Label">{{ field.Label }}</span>
</label>

<div *ngFor="let choice of field.Choices">
    <label>
        <input type="radio" name="{{ field.Name }}" value="{{ choice.Value }}"  [checked]="choice.Selected" disabled />
        {{ choice.Label }}
    </label>
</div>`
})

export class RadioViewerComponent {
    @Input() field: FormField;
}
