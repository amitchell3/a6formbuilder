import { Injectable, Input } from '@angular/core';
import { FormBuilderForm, FormField } from './app.component';

@Injectable({
    providedIn: 'root',
})

export class HtmlService {
    constructor() { }

    generateHtml(form: FormBuilderForm): string {
        let variables = {
            formId: form.id,
            formDomId: this.getId(form.ActionCode),
            enctype: ' '
        };

        form.Fields.forEach(field => {
            if (field.FieldType == "FileUpload" && variables.enctype != 'enctype="multipart/form-data"') {
                variables.enctype = ' enctype="multipart/form-data" ';
            }
        });

        var html = `<form id="${variables.formDomId}" action="https://${form.Host}/save"${variables.enctype}class="${form.CssClass}" method="post">${this.indent(1)}<input type="hidden" name="FormId" value="${variables.formId}" />${this.indent(1)}<input type="hidden" name="ActivityCode" value="${form.ActionCode}" />`;

        if (form.SuccessAction == "redirect") {
            html += `${this.indent(1)}<input type="hidden" name="SuccessUrl" value="${form.SuccessUrl}" />`
        }

        html += `${this.renderFields(form.Fields)}${this.indent(1)}<button type="submit">${form.SubmitButtonText}</button>\n</form>`

        if (form.SuccessAction == "showContent") {
            html += this.buildSuccessContent(form);
        }

        return html;
    }

    private getId(name): string {
        return name.replace(/[^a-zA-Z0-9]+/g, '').toLowerCase();
    }

    private indent(n) {
        var space = "    ";
        var newIndent = "";
        for (var i = 0; i < n; i++) {
            newIndent += space;
        }
        return "\n" + newIndent;
    }

    private buildSuccessContent(form: FormBuilderForm): string {
        var content = form.SuccessContentFormat == "text" ? `<p>${form.SuccessContent}</p>` : form.SuccessContent;
        return `\n<div id="${this.getId(form.ActionCode)}-response" style="display:none;"> ${content} </div>`;
    }

    private renderFields(fields: FormField[]): string {
        let html: string = "";
        fields.forEach(field => {
            switch (field.FieldType) {
                case "FirstName":
                case "LastName":
                case "PostalCode":
                case "EmailAddress":
                case "StreetAddress":
                case "City":
                case "Phone":
                case "TextBox":
                case "FileUpload":
                    html += this.renderTextbox(field);
                    break;
                case "Comments":
                case "TextArea":
                    html += this.renderTextarea(field);
                    break;
                case "EmailOptIn":
                case "SingleCheckBox":
                    html += this.renderSingleCheckBox(field);
                    break;
                case "RadioButtonList":
                    html += this.renderRadioButtonList(field);
                    break;
                case "State":
                    html += this.renderStatesList(field);
                    break;
                case "DropDownList":
                    html += this.renderDropDownList(field);
                    break;
                case "CheckBoxList":
                    html += this.renderCheckBoxList(field);
                    break;
            }
        });
        return html;
    }

    private renderTextbox(field: FormField): string {
        let variables = {
            inputType: "text",
            id: this.getId(field.Name),
            required: field.Required ? 'class="required" required' : '',
            requiredHint: field.Required ? '<abbr title="required">*</abbr>' : ''
        };


        if (field.FieldType == "EmailAddress") {
            variables.inputType = "email";
        }

        if (field.FieldType == "Phone") {
            variables.inputType = "tel";
        }

        if (field.FieldType == "FileUpload") {
            variables.inputType = "file";
        }

        return `${this.indent(1)}<div><label for="${variables.id}">${field.Label}${variables.requiredHint}</label><input type="${variables.inputType}" id="${variables.id}" name="${field.Name}" ${variables.required}/></div>`
    }

    private renderTextarea(field: FormField): string {
        let variables = {
            id: this.getId(field.Name),
            required: field.Required ? 'class="required" required' : '',
            requiredHint: field.Required ? '<abbr title="required">*</abbr>' : ''
        };

        return `${this.indent(1)}<div><label for="${variables.id}">${field.Label}${variables.requiredHint}</label><textarea id="${variables.id}" name="${field.Name}" rows="${field.Rows}" ${variables.required}></textarea></div>`
    }

    private renderRadioButtonList(field: FormField): string {
        let html = `${this.indent(1)}<div>${this.indent(2)}<label>${field.Label}</label>${this.indent(2)}<div>`

        field.Choices.forEach((choice, i) => {
            let variables = {
                id: this.getId(field.Name + i),
                checked: choice.Selected ? " checked" : ""
            };

            html += `${this.indent(3)}<label for="${variables.id}"><input type="radio" id="${variables.id}" name="${field.Name}" value="${choice.Value}"${variables.checked} />${choice.Label}</label>`
        });

        html += `${this.indent(2)}</div>${this.indent(1)}</div>`

        return html;
    }

    private renderCheckBoxList(field: FormField): string {
        let html = `${this.indent(1)}<div class="form-field radio-list">${this.indent(2)}<label>${field.Label}</label>${this.indent(2)}<div>`

        field.Choices.forEach((choice, i) => {
            let variables = {
                id: this.getId(field.Name + i),
                value: field.Name + "_" + choice.Value,
                checked: choice.Selected ? " checked" : ""
            };

            html += `${this.indent(3)}<label for="${variables.id}"><input type="checkbox" id="${variables.id}" name="${field.Name}" value="${variables.value}"${variables.checked} />${choice.Label}</label>`
        });

        html += `${this.indent(2)}</div>${this.indent(1)}</div>`

        return html;
    }

    private renderDropDownList(field: FormField): string {
        let variables = {
            id: this.getId(field.Name),
            required: field.Required ? 'class="required" required' : '',
            requiredHint: field.Required ? '<abbr title="required">*</abbr>' : ''
        };

        let html = `${this.indent(1)}<div class="form-field drop-down-list">${this.indent(2)}<label for="${variables.id}">${field.Label}${variables.requiredHint}</label>${this.indent(2)}<div>${this.indent(3)}<select id="${variables.id}" name="${field.Name}" ${variables.required} >`

        var defaultOptionSelected = false;

        for (var i = 0, len = field.Choices.length; i < len; i++) {
            if (field.Choices[i].Selected === true) {
                defaultOptionSelected = true;
                break;
            }
        }

        if (!defaultOptionSelected) {
            html += `${this.indent(4)} <option disabled selected></option>`;
        }

        field.Choices.forEach((choice, i) => {
            let choiceVariables = {
                selected: choice.Selected ? " selected" : ""
            };

            html += `${this.indent(4)}<option value="${choice.Value}"${choiceVariables.selected}>${choice.Label}</option>`;
        });

        html += `${this.indent(3)}</select>${this.indent(2)}</div>${this.indent(1)}<div>`

        return html;
    }

    private renderSingleCheckBox(field: FormField): string {
        let variables = {
            id: this.getId(field.Name),
            selected: field.Selected,
            checked: field.Selected ? " checked" : ""
        };

        return `${this.indent(1)}<div class="form-field single-check-box"><label for="${variables.id}"><input type="checkbox" id="${variables.id}" name="${field.Name}" value="true"${variables.checked} />${field.Label}</label>${this.indent(1)}</div>`;
    }

    private renderStatesList(field: FormField): string {
        field.Choices = [
            {
                Label: "AK",
                Value: "AK",
                Selected: false
            },
            {
                Label: "AL",
                Value: "AL",
                Selected: false
            },
            {
                Label: "AR",
                Value: "AR",
                Selected: false
            },
            {
                Label: "AZ",
                Value: "AZ",
                Selected: false
            },
            {
                Label: "CA",
                Value: "CA",
                Selected: false
            },
            {
                Label: "CO",
                Value: "CO",
                Selected: false
            },
            {
                Label: "CT",
                Value: "CT",
                Selected: false
            },
            {
                Label: "DC",
                Value: "DC",
                Selected: false
            },
            {
                Label: "DE",
                Value: "DE",
                Selected: false
            },
            {
                Label: "FL",
                Value: "FL",
                Selected: false
            },
            {
                Label: "GA",
                Value: "GA",
                Selected: false
            },
            {
                Label: "HI",
                Value: "HI",
                Selected: false
            },
            {
                Label: "IA",
                Value: "IA",
                Selected: false
            },
            {
                Label: "ID",
                Value: "ID",
                Selected: false
            },
            {
                Label: "IL",
                Value: "IL",
                Selected: false
            },
            {
                Label: "IN",
                Value: "IN",
                Selected: false
            },
            {
                Label: "KS",
                Value: "KS",
                Selected: false
            },
            {
                Label: "KY",
                Value: "KY",
                Selected: false
            },
            {
                Label: "LA",
                Value: "LA",
                Selected: false
            },
            {
                Label: "MA",
                Value: "MA",
                Selected: false
            },
            {
                Label: "MD",
                Value: "MD",
                Selected: false
            },
            {
                Label: "ME",
                Value: "ME",
                Selected: false
            },
            {
                Label: "MI",
                Value: "MI",
                Selected: false
            },
            {
                Label: "MN",
                Value: "MN",
                Selected: false
            },
            {
                Label: "MO",
                Value: "MO",
                Selected: false
            },
            {
                Label: "MS",
                Value: "MS",
                Selected: false
            },
            {
                Label: "MT",
                Value: "MT",
                Selected: false
            },
            {
                Label: "NC",
                Value: "NC",
                Selected: false
            },
            {
                Label: "ND",
                Value: "ND",
                Selected: false
            },
            {
                Label: "NE",
                Value: "NE",
                Selected: false
            },
            {
                Label: "NH",
                Value: "NH",
                Selected: false
            },
            {
                Label: "NJ",
                Value: "NJ",
                Selected: false
            },
            {
                Label: "NM",
                Value: "NM",
                Selected: false
            },
            {
                Label: "NV",
                Value: "NV",
                Selected: false
            },
            {
                Label: "NY",
                Value: "NY",
                Selected: false
            },
            {
                Label: "OH",
                Value: "OH",
                Selected: false
            },
            {
                Label: "OK",
                Value: "OK",
                Selected: false
            },
            {
                Label: "OR",
                Value: "OR",
                Selected: false
            },
            {
                Label: "PA",
                Value: "PA",
                Selected: false
            },
            {
                Label: "RI",
                Value: "RI",
                Selected: false
            },
            {
                Label: "SC",
                Value: "SC",
                Selected: false
            },
            {
                Label: "SD",
                Value: "SD",
                Selected: false
            },
            {
                Label: "TN",
                Value: "TN",
                Selected: false
            },
            {
                Label: "TX",
                Value: "TX",
                Selected: false
            },
            {
                Label: "UT",
                Value: "UT",
                Selected: false
            },
            {
                Label: "VT",
                Value: "VT",
                Selected: false
            },
            {
                Label: "VA",
                Value: "VA",
                Selected: false
            },
            {
                Label: "WA",
                Value: "WA",
                Selected: false
            },
            {
                Label: "WI",
                Value: "WI",
                Selected: false
            },
            {
                Label: "WV",
                Value: "WV",
                Selected: false
            },
            {
                Label: "WY",
                Value: "WY",
                Selected: false
            },
            {
                Label: "AA",
                Value: "AA",
                Selected: false
            },
            {
                Label: "AE",
                Value: "AE",
                Selected: false
            },
            {
                Label: "AP",
                Value: "AP",
                Selected: false
            }
        ];

        return this.renderDropDownList(field);
    }
}