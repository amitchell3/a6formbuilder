import { Component, Input } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-textbox-editor',
    template: `<section provide-parent-form>
    <app-common-fields [field]="field"></app-common-fields>

    <div>
        <label><input type="checkbox" name="fieldRequired" [(ngModel)]="field.Required" /> Required Field</label>
    </div>
</section>`
})

export class TextBoxEditorComponent {
    @Input() field: FormField;
}
