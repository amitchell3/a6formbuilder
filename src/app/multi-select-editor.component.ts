import { Component, Input } from '@angular/core';
import { FormField, FieldChoice } from './app.component';

@Component({
    selector: 'app-multi-select-editor',
    template: `<section provide-parent-form>
    <app-common-fields [field]="field" ></app-common-fields>

    <div ng-show="field.FieldType == 'DropDownList'">
        <label><input type="checkbox" name="fieldRequired" [(ngModel)]="field.Required" /> Required Field</label>
    </div>

    <table>
        <thead>
            <tr>
                <th style="text-align: center;">Checked</th>
                <th>Label</th>
                <th>Value</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        <tr *ngFor="let choice of field.Choices; let i = index">
                <td>
                    <input type="checkbox" name="{{'selected' + i}}" [(ngModel)]="choice.Selected" value="true" />
                </td>
                <td>
                    <input
                        type="text"
                        required
                        name="{{'labelName' + i}}"
                        ng-init="labelName = 'choiceLabel' + $index"
                        [(ngModel)]="choice.Label" />
                </td>
                <td>
                    <input
                        type="text"
                        required
                        name="{{'valueName' + i}}"
                        ng-init="valueName = 'choiceValue' + $index"
                        [(ngModel)]="choice.Value" />
                </td>
                <td>
                    <button (click)="addChoice(i + 1)"><i>+</i></button>
                    <button (click)="removeChoice(choice)" ng-hide="$first && $last"><i>-</i></button>
                </td>
            </tr>
        </tbody>
    </table>
</section>`
})

export class MultiSelectEditorComponent {
    @Input() field: FormField;

    addChoice(index) {
        if (typeof index === 'undefined') {
            index = this.field.Choices.length + 1;
        }

        this.field.Choices.splice(index, 0, new FieldChoice('', '', false));
    }

    removeChoice(choice) {
        const index = this.field.Choices.indexOf(choice);
        this.field.Choices.splice(index, 1);
    }
}
