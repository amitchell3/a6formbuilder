import { Component, Input, OnInit } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-drop-down-viewer',
    template: `<div>
    <label>
        <span *ngIf="!field.Label">[Enter Label]</span>
        <span *ngIf="field.Label">{{ field.Label }}</span>
</label>

<select disabled>
    <option></option>
    <option *ngFor="let choice of field.Choices" ng-selected="choice.Selected">{{ choice.Label }}</option>
</select>
</div>`
})

export class DropDownViewerComponent {
    @Input() field: FormField;
}
