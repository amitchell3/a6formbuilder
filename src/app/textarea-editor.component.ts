import { Component, Input } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-textarea-editor',
    template: `<section provide-parent-form>
    <app-common-fields [field]="field"></app-common-fields>

    <div>
        <label>Number of rows</label>
        <input type="number" name="fieldRows" [(ngModel)]="field.Rows" min="2" />
    </div>

    <div>
        <label><input type="checkbox" name="fieldRequired" [(ngModel)]="field.Required" /> Required Field</label>
    </div>
</section>`
})

export class TextAreaEditorComponent {
    @Input() field: FormField;
}
