import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'app-html-viewer',
    template: `<div style="margin: 8px auto; padding: 8px; border: 1px solid #ccc; text-align: center;">
    <button (click)="close()">Close</button><br>
    <textarea #codeArea style="width: 900px; height: 600px;display: block; margin: 0 auto; font-family: 'Lucida Console', Monaco, monospace" spellcheck="false" readonly>{{ html }}</textarea><br>
    <button (click)="selectCode()">Select All</button>
    <button (click)="copyCode()">Copy HTML to Clipboard</button>
</div>`
})

export class HtmlViewerComponent {
    @Input() html: string;
    @Output() showHtml: EventEmitter<boolean>;
    @ViewChild('codeArea') codeArea: ElementRef;

    constructor() {
        this.showHtml = new EventEmitter<boolean>();
    }

    close() {
        this.showHtml.emit(false);
    }

    selectCode() {
        this.codeArea.nativeElement.focus();
        this.codeArea.nativeElement.select();
    }

    copyCode() {
        this.codeArea.nativeElement.focus();
        this.codeArea.nativeElement.select();
        document.execCommand('copy');
        window.getSelection().removeAllRanges();
    }
}
