import { Component, Input, OnInit } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-file-upload-viewer',
    template: `<div>
    <label>
        <span *ngIf="!field.Label">[Enter Label]</span>
        <span *ngIf="field.Label">{{ field.Label }}</span>
    </label>
    <div>
        <button disabled>Choose File</button>
        <span>No file chosen</span>
    </div>
</div>`
})

export class FileUploadViewerComponent {
    @Input() field: FormField;
}
