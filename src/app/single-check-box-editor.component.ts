import { Component, Input } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-single-check-box-editor',
    template: `<section provide-parent-form>
    <app-common-fields [field]="field" ></app-common-fields>

    <div>
        <label><input type="checkbox" name="fieldSelected" [(ngModel)]="field.Selected" /> Box is checked by default</label>
    </div>
</section>`
})

export class SingleCheckBoxEditorComponent {
    @Input() field: FormField;
}
