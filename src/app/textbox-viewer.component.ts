import { Component, Input, OnInit } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-textbox-viewer',
    template: `<div>
    <label>
        <span *ngIf="!field.Label">[Enter Label]</span>
        <span *ngIf="field.Label">{{ field.Label }}</span>
    </label>
    <input type="text" name="{{ field.FieldType }}" disabled />
</div>`
})

export class TextBoxViewerComponent {
    @Input() field: FormField;
}
