import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CommonFieldsComponent } from './common-fields.component';
import { TextBoxViewerComponent } from './textbox-viewer.component';
import { TextAreaViewerComponent } from './textarea-viewer.component';
import { SingleCheckBoxViewerComponent } from './single-check-box-viewer.component';
import { RadioViewerComponent } from './radio-viewer-component';
import { DropDownViewerComponent } from './drop-down-viewer.component';
import { CheckBoxViewerComponent } from './checkbox-viewer.component';
import { FileUploadViewerComponent } from './file-upload-viewer.component';
import { StandardFieldEditorComponent } from './standard-field-editor.component';
import { StandardFieldLabelEditorComponent } from './standard-field-label-editor.component';
import { StandardFieldTextAreaEditorComponent } from './standard-field-textarea-editor.component';
import { TextBoxEditorComponent } from './textbox-editor.component';
import { TextAreaEditorComponent } from './textarea-editor.component';
import { SingleSelectEditorComponent } from './single-select-editor.component';
import { SingleCheckBoxEditorComponent } from './single-check-box-editor.component';
import { MultiSelectEditorComponent } from './multi-select-editor.component';
import { HtmlViewerComponent } from './html-viewer.component';
import { ProvideParentFormDirective } from './provide-parent-form.directive';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CommonFieldsComponent,
    TextBoxViewerComponent,
    TextAreaViewerComponent,
    RadioViewerComponent,
    SingleCheckBoxViewerComponent,
    DropDownViewerComponent,
    CheckBoxViewerComponent,
    FileUploadViewerComponent,
    StandardFieldEditorComponent,
    StandardFieldLabelEditorComponent,
    StandardFieldTextAreaEditorComponent,
    TextBoxEditorComponent,
    TextAreaEditorComponent,
    SingleSelectEditorComponent,
    SingleCheckBoxEditorComponent,
    MultiSelectEditorComponent,
    HtmlViewerComponent,
    ProvideParentFormDirective
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
