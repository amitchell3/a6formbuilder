import { Component, Input } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-common-fields',
    template: `<div provide-parent-form>
    <div>
        <label>Field Label</label>
        <textarea required name="fieldLabel" [(ngModel)]="field.Label" (input)="onLabelChange()" rows="2"></textarea>
    </div>

    <div>
        <label>Field Name</label>
        <input type="text" required name="fieldName" [(ngModel)]="field.Name" [pattern]="'^[a-zA-Z0-9_-]*$'" (blur)="onNameBlur()" (keyup)="field.Duplicate = false" [class.duplicate]="field.Duplicate" />
    </div>
</div>`
})

export class CommonFieldsComponent {
    @Input() field: FormField;

    nameInputTouched: boolean;

    constructor() {
        this.nameInputTouched = false;
    }

    onLabelChange() {
        if (this.nameInputTouched) {
            return;
        }

        if (this.field.Label && this.field.Label != '') {
            this.field.Name = this.field.Label.replace(/[^a-z0-9_-]+/gi, "");
            this.field.Duplicate = false;
        }
    }

    onNameBlur() {
        if (this.nameInputTouched && this.field.Name == '') {
            this.nameInputTouched = false;
        } else {
            this.nameInputTouched = true
        }
    }
}
