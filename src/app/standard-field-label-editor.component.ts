import { Component, Input } from '@angular/core';
import { FormField } from './app.component';

@Component({
    selector: 'app-standard-field-label-editor',
    template: `<section>

    <div>
        <label>Field Label</label>
        <inputtype="text" [(ngModel)]="field.Label" disabled />
    </div>

    <div>
        <label>Field Name</label>
        <inputtype="text" [(ngModel)]="field.Name" disabled />
    </div>

    <div>
        <label><input type="checkbox" [(ngModel)]="field.Required" /> Required Field</label>
    </div>

    <button (click)="removeField()">Remove Field</button>
</section>`
})

export class StandardFieldLabelEditorComponent {
    @Input() field: FormField;
}
