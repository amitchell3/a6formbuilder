import { Component, Input, OnInit } from '@angular/core';
import { FormField } from './app.component';
import { findSafariExecutable } from 'selenium-webdriver/safari';

@Component({
    selector: 'app-checkbox-viewer',
    template: `<div>
    <label>
        <span *ngIf="!field.Label">[Enter Label]</span>
        <span *ngIf="field.Label">{{ field.Label }}</span>
    </label>

    <div *ngFor="let choice of field.Choices">
        <label>
            <input type="checkbox" name="{{ field.Name }}" value="{{ choice.value }}" ng-checked="choice.selected" disabled />
            {{ choice.Label }}
        </label>
    </div>
</div>`
})

export class CheckBoxViewerComponent {
    @Input() field: FormField;
}
